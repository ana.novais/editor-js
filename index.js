import EditorJs from '@editorjs/editorjs';
import Header from '@editorjs/header';
import List from '@editorjs/list';
import Embed from '@editorjs/embed';


const editor = new EditorJs({
    holderId: 'editorjs',
    
    tools: {
        header: {
            class: Header,
            inlineToobar: ['link']
        },
        list: {
            class: List,
            inlineToobar: [
                'link',
                'bold'
            ]
        },
        embed: {
            class: Embed,
            inlineToobar: false,
            config: {
                services: {
                    youtube: true,
                    coub: true
                }
            }
        },
    }
})

let saveBtn = document.querySelector('button');

saveBtn.addEventListener('click', function() {
    editor.save().then((outputData) => {
        console.log('Article data: ', outputData);
    }).catch((erro) => {
        console.log('Saving failed: ', error);
    })
})